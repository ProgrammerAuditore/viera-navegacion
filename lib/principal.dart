import 'package:flutter/material.dart';
import 'package:navegacion/segunda_page.dart';

class Principal extends StatelessWidget {
  const Principal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Main App'),),
      body: Center(
        child: ElevatedButton(
          child:  Text('Ir a segunda pagina'), 
          onPressed: (){
            Navigator.push(
              context, 
              MaterialPageRoute(
                builder: (BuildContext context) => SegundaPagina()
              )
            ); // Navigator
          }
        ),
      )  
    );
  }
}
