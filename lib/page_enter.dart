import 'package:flutter/material.dart';
import 'package:navegacion/principal.dart';

class PageEnter extends StatelessWidget {
  const PageEnter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child:  Text('Loggin'), 
          onPressed: (){
            Navigator.pushReplacement(
              context, 
              MaterialPageRoute(
                builder: (BuildContext context) => Principal()
              )
            ); // Navigator
          }
        ),
      ),
    );
  }
}