import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:navegacion/page_enter.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('Ejercicio'),
          leading: Icon(
            Icons.access_alarm,
            color: Colors.amber,
          ),
          actions: [
            IconButton(
              onPressed: (){}, 
              icon: Icon(
                Icons.abc_rounded,
                color: Colors.black,
              ),
              )
          ]
        ),
        body: Center(
          child: Container(
            child: Text('Hola mundo')
          )
        ),
      ),
    );
  }
}
